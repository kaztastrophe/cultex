extends Node3D


@onready var target_scene = preload("res://elements/target.tscn")
var spawns


func _ready():
	spawns = get_tree().get_nodes_in_group('spawns')
	var max = min(Global.max_spawns, spawns.size())
	for _id in range(0, randi_range(2, max)):
		_spawn()


func _spawn():
	var id = randi_range(0, spawns.size()-1)
	if spawns[id].can_spawn():
		var target = target_scene.instantiate()
		add_child(target)
		target.global_position = spawns[id].global_position


func _on_timer_timeout():
	var target_count = get_tree().get_nodes_in_group('targets').size()
	if target_count < Global.max_spawns:
		_spawn()
	$Timer.start((target_count + 1) * 5)
