class_name Target
extends CharacterBody3D


const SPEED = 1.0

@onready var nav = $NavigationAgent3D

var skins = [
	Color.ANTIQUE_WHITE,
	Color.BLANCHED_ALMOND,
	Color.BURLYWOOD,
	Color.PERU,
	Color.SADDLE_BROWN,
	Color.SIENNA,
	Color.TAN,
	Color.hex(0x8d5524ff),
	Color.hex(0x594545ff),
	Color.hex(0x1c0a00ff),
	Color.hex(0x361500ff),
	Color.hex(0xc68b59ff),
	Color.hex(0xf2efeeff),
	Color.hex(0xefe6ddff),
	Color.hex(0xebd3c5ff),
	Color.hex(0xd7b6a5ff),
	Color.hex(0x9f7967ff),
	Color.hex(0x70361cff),
	Color.hex(0x714937ff),
	Color.hex(0x65371eff),
	Color.hex(0x492816ff),
	Color.hex(0x321b0fff),
	Color.hex(0xbf9169ff),
	Color.hex(0x8c644dff),
	Color.hex(0x593123ff),
]


func _ready():
	_set_colors()
	_set_random_navigation_target()


func _set_colors():
	var skin = StandardMaterial3D.new()
	skin.albedo_color = skins[randi_range(0, skins.size()-1)]
	%Body.set_surface_override_material(0, skin)
	
	var shirt = StandardMaterial3D.new()
	shirt.albedo_color = Color(randf(), randf(), randf())
	%Body.set_surface_override_material(1, shirt)
	
	var pants = StandardMaterial3D.new()
	pants.albedo_color = Color(randf(), randf(), randf())
	%Body.set_surface_override_material(2, pants)


func _exit_tree():
	%Body.set_surface_override_material(0, null)
	%Body.set_surface_override_material(1, null)
	%Body.set_surface_override_material(2, null)


func _physics_process(_delta):
	if %Vision.has_overlapping_bodies():
		if _observe():
			return
		
	
	var next = nav.get_next_path_position()
	if global_position.is_equal_approx(next):
		_set_random_navigation_target()
	else:
		look_at(next)
	var new_velocity = (next - global_position).normalized() * SPEED
	nav.set_velocity(new_velocity)


func _observe():
	var player = %Vision.get_overlapping_bodies()[0]
	if !player.has_body:
		return false
	
	look_at(player.position)
	var sight = %LineOfSight.get_collider()
	if sight is Player:
		Global.caught()
	return true


func be_attacked():
	queue_free()


func _on_navigation_agent_3d_velocity_computed(safe_velocity):
	velocity = velocity.move_toward(safe_velocity, 0.1)
	move_and_slide()


func _set_random_navigation_target():
	var offset = Vector3.ZERO
	offset.x = randf_range(1.0,10.0)
	offset.z = randf_range(1.0,10.0)
	
	nav.target_position = global_position+offset
