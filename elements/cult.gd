class_name CultBuilding
extends StaticBody3D


@onready var playback = $AnimationTree.get('parameters/playback')
var is_open = true


func _ready():
	playback.travel('Open')


func _open_garage(_van: Node3D):
	if is_open:
		return
	playback.travel('Open')
	is_open = true


func _close_garage(_van: Node3D):
	if !is_open:
		return
	if %GarageOpener.has_overlapping_bodies() or %Delivery.has_overlapping_bodies():
		return
	playback.travel('Close')
	is_open = false


func _deliver_targets(van: Node3D):
	Global.targets_delivered += van.remove_bodies()
