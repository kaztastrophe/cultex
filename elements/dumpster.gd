class_name Dumpster
extends StaticBody3D


const MAX_BODIES = 3

@onready var playback = $AnimationTree.get('parameters/playback')

var bodies = 0


func add_body():
	playback.travel('Open')
	if bodies >= MAX_BODIES:
		return false
	bodies += 1
	return true


func retrieve_body():
	playback.travel('Open')
	if bodies <= 0:
		return false
	bodies -= 1
	return true
