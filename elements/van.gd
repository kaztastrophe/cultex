class_name Van
extends CharacterBody3D


signal van_entered(bodies: int)
signal van_exited


const SPEED = 7.0
const SPACE = 7

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var bodies: int = 0


func _ready():
	van_entered.connect(Global.update_van_ui)
	van_exited.connect(Global.update_van_ui)


func _physics_process(delta):
	if global_position.y < -5:
		Global.caught()
		return
	
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	
	if Input.is_action_just_pressed("action"):
		_exit_van()
		return

	# Get the input direction and handle the movement/deceleration.
	var input_dir = Input.get_axis("move_forward", "move_back")
	var direction = (transform.basis * Vector3(input_dir, 0, 0)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
	
	# Get the input rotation and handle it.
	var input_rot = Input.get_axis("turn_right", "turn_left")
	rotate_y(deg_to_rad(input_rot * Global.turn_sensitivity * -input_dir))
	
	move_and_slide()


func enter_van():
	$Camera3D.current = true
	set_physics_process(true)
	van_entered.emit(bodies)


func _exit_van():
	set_physics_process(false)
	$Exit.monitoring = true
	
	var player_position = $Exit/CollisionShape3D.position
	if $Exit.has_overlapping_bodies():
		player_position += Vector3(0,0,-3)
	Global.add_player(to_global(player_position), global_position)
	van_exited.emit()
	
	$Exit.monitoring = false
	$Camera3D.current = false


func add_body():
	if bodies >= SPACE:
		return false
	bodies += 1
	return true


func remove_bodies():
	var temp = bodies
	bodies = 0
	return temp
