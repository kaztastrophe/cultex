extends Area3D


func _ready():
	if has_overlapping_bodies():
		queue_free()
	monitoring = false


func can_spawn():
	monitoring = true
	var obstacle = has_overlapping_bodies()
	monitoring = false
	return !obstacle
