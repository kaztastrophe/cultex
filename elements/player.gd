class_name Player
extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 4.5

enum Actions {
	JUMP,
	ATTACK,
	DRIVE,
	LOAD,
	RETRIEVE,
	NONE,
}

enum Animations {
	IDLE,
	WALK,
	BODY_CARRY,
}

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

@onready var playback = $AnimationTree.get('parameters/playback')

var mouse_rotation: bool = false

var has_body = false


func _physics_process(delta):
	if global_position.y < -5:
		Global.caught()
		return
		
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("action"):
		var action = _get_possible_action()
		if action == Actions.JUMP:
			velocity.y = JUMP_VELOCITY
			_update_animation(Animations.IDLE)
		elif action != Actions.NONE:
			_act(action)

	# Get the input direction and handle the movement/deceleration.
	var speed = SPEED
	if has_body:
		speed /= 2
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)
	
	# Get the input rotation and handle it.
	# Verifies if player is not moving sideways before rotating.
	if input_dir.x == 0:
		var input_rot = Input.get_axis("turn_right", "turn_left")
		rotate_y(deg_to_rad(input_rot * Global.turn_sensitivity))
	
	move_and_slide()
	if input_dir:
		_update_animation(Animations.WALK)
	else:
		_update_animation(Animations.IDLE)


func _input(event):
	if event is InputEventMouseMotion and mouse_rotation:
		rotate_y(deg_to_rad(-event.relative.x * Global.mouse_sensitivity))
	elif event is InputEventMouseButton and event.button_mask == 0:
		mouse_rotation = !mouse_rotation


func _get_possible_action():
	var interactibles = %Interaction.get_overlapping_bodies()
	if interactibles.size() > 0:
		if interactibles[0] is Van and has_body:
			return Actions.LOAD
		elif interactibles[0] is Van:
			return Actions.DRIVE
		elif interactibles[0] is Dumpster and has_body:
			return Actions.LOAD
		elif interactibles[0] is Dumpster and !has_body:
			return Actions.RETRIEVE
		elif interactibles[0] is Target and !has_body:
			return Actions.ATTACK
	
	if is_on_floor() and !has_body:
		return Actions.JUMP
	
	return Actions.NONE


func _act(action: Actions):
	var target = %Interaction.get_overlapping_bodies()[0]
	match action:
		Actions.ATTACK:
			target.be_attacked()
			has_body = true
			_update_animation(Animations.BODY_CARRY)
		Actions.LOAD:
			var success = target.add_body()
			if success:
				has_body = false
				_update_animation(Animations.IDLE)
		Actions.DRIVE:
			target.enter_van()
			queue_free()
		Actions.RETRIEVE:
			var success = target.retrieve_body()
			if success:
				has_body = true
				_update_animation(Animations.BODY_CARRY)


func _update_animation(animation: Animations):
	match animation:
		Animations.IDLE:
			playback.travel('Idle')
		Animations.WALK:
			playback.travel('Walk')
#		Animations.BODY_CARRY:
#			playback.travel('BodyCarry')
