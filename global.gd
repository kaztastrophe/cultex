extends Node


signal delivery(delivered: int)
signal van_entered(bodies: int)
signal van_exited


@onready var player_scene = preload("res://elements/player.tscn")

var turn_sensitivity: float = 1.3 :
	set = _set_turn_sensitivity
var mouse_sensitivity: float = 0.3 :
	set = _set_mouse_sensitivity
var max_goal: int = 10 :
	set = _set_max_goal
var max_spawns: int = 15 :
	set = _set_max_spawns

var target_goal: int
var targets_delivered: int = 0 :
	set = _deliver_targets


func _ready():
	target_goal = randi_range(1, max_goal)


func _set_turn_sensitivity(new: float):
	if new <= 0:
		turn_sensitivity = 0.1
	elif new > 2:
		turn_sensitivity = 2.0
	else:
		turn_sensitivity = new


func _set_mouse_sensitivity(new: float):
	if new <= 0:
		mouse_sensitivity = 0.1
	elif new > 2:
		mouse_sensitivity = 2.0
	else:
		mouse_sensitivity = new


func _set_max_goal(new: int):
	if new <= 0:
		max_goal = 1
	elif new > 100:
		max_goal = 100
	else:
		max_goal = new


func _set_max_spawns(new: int):
	if new <= 0:
		max_spawns = 1
	elif new > 100:
		max_spawns = 100
	else:
		max_spawns = new


func _deliver_targets(count: int):
	targets_delivered = count
	delivery.emit(count)
	if targets_delivered >= target_goal:
		win()


func caught():
	SceneManager.end_game(false)


func win():
	SceneManager.end_game(true)


func add_player(player_position: Vector3, van_position: Vector3):
	var player = player_scene.instantiate()
	var level = get_tree().root.get_node('Level')
	level.add_child(player)
	player.global_position = player_position
	player.look_at(van_position)


func update_van_ui(body_count: int = -1):
	if body_count >= 0:
		van_entered.emit(body_count)
	else:
		van_exited.emit()
