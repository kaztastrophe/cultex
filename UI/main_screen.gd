extends Control


var show_options: bool = false


func new_game_button():
	SceneManager.start_game()
	queue_free()


func options_button():
	show_options = !show_options
	%StartGameButton.visible = !show_options
	%OptionsMenu.visible = show_options
	if show_options:
		%OptionsButton.text = 'Back'
	else:
		%OptionsButton.text = 'Options'
