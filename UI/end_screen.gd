extends Control


func _unhandled_key_input(event):
	if Input.is_action_just_pressed('ui'):
		queue_free()
		SceneManager.main_menu()


func set_label(text: String):
	%Label.text = text
