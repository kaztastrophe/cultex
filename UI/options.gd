extends VBoxContainer


@export var main_menu_button: bool = true :
	set = _toggle_main_menu_button


func _ready():
	%TurnSensitivity.value = Global.turn_sensitivity
	%TurnSensitivityValue.text = str(Global.turn_sensitivity)
	
	%MouseSensitivity.value = Global.mouse_sensitivity
	%MouseSensitivityValue.text = str(Global.mouse_sensitivity)
	
	%MaxGoal.value = Global.max_goal
	%MaxGoalValue.text = str(Global.max_goal)
	
	%MaxSpawns.value = Global.max_spawns
	%MaxSpawnsValue.text = str(Global.max_spawns)


func go_to_main_menu():
	SceneManager.main_menu()


func change_turn_sensitivity(changed: bool):
	if changed:
		Global.turn_sensitivity = %TurnSensitivity.value
		%TurnSensitivityValue.text = str(Global.turn_sensitivity)


func update_turn_sensitivity_value(value: float):
	%TurnSensitivityValue.text = str(value)


func change_mouse_sensitivity(changed: bool):
	if changed:
		Global.mouse_sensitivity = %MouseSensitivity.value
		%MouseSensitivityValue.text = str(Global.mouse_sensitivity)


func update_mouse_sensitivity_value(value: float):
	%MouseSensitivityValue.text = str(value)


func change_max_goal(changed: bool):
	if changed:
		Global.max_goal = %MaxGoal.value
		%MaxGoalValue.text = str(Global.max_goal)


func update_max_goal_value(value: float):
	%MaxGoalValue.text = str(value)


func change_max_spawns(changed: bool):
	if changed:
		Global.max_spawns = %MaxSpawns.value
		%MaxSpawnsValue.text = str(Global.max_spawns)


func update_max_spawns_value(value: float):
	%MaxSpawnsValue.text = str(value)


func _toggle_main_menu_button(is_visible: bool):
	main_menu_button = is_visible
	$MainMenu.visible = is_visible
