extends Control


func _ready():
	Global.van_entered.connect(_show_van_ui)
	Global.van_exited.connect(_hide_van_ui)
	Global.delivery.connect(_update_delivery_ui)
	_update_delivery_ui(0)
	get_tree().paused = true


func _unhandled_key_input(event):
	if Input.is_action_just_pressed('ui'):
		_toggle_pause()


func _toggle_pause():
	var pause = !get_tree().paused
	%Pause.visible = pause
	get_tree().paused = pause


func _show_van_ui(count: int):
	%Van.visible = true
	%Van/Label.text = str(count)


func _hide_van_ui():
	%Van.visible = false


func _update_delivery_ui(count: int):
	%Van/Label.text = '0'
	var delivery_text = '%s / %s' % [count, Global.target_goal]
	%Delivered/Label.text = delivery_text
