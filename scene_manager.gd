extends Node


@onready var main_menu_scene = preload("res://UI/main_screen.tscn")
@onready var level_scene = preload("res://levels/level.tscn")
@onready var end_menu_scene = preload("res://UI/end_screen.tscn")


func start_game():
	var game = level_scene.instantiate()
	get_tree().root.add_child(game)


func end_game(victory: bool):
	get_tree().paused = true
	var menu = end_menu_scene.instantiate()
	if victory:
		menu.set_label('You won!')
	else:
		menu.set_label('You lost.')
	get_tree().root.add_child(menu)


func main_menu():
	var level = get_tree().root.get_node('Level')
	if level:
		level.process_mode = Node.PROCESS_MODE_DISABLED
		level.queue_free()
	var menu = main_menu_scene.instantiate()
	get_tree().root.add_child(menu)
	get_tree().paused = false
